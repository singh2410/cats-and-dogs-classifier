#!/usr/bin/env python
# coding: utf-8

# # Cats vs dogs classifier (Testing)
# #By- Aarush Kumar
# #Dated: June 02,2021

# In[1]:


import cv2
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


# In[3]:


Categories=['Doggo','Caties']
image=r'/home/aarush100616/Downloads/Projects/Cats & Dogs Classifier/332.jpg'
image


# In[4]:


def prepare(image):
    img_size=100
    img_array=cv2.imread(image,cv2.IMREAD_GRAYSCALE)
    new_array=cv2.resize(img_array,(img_size,img_size))
    return new_array.reshape(-1,img_size,img_size,1)


# In[8]:


model = tf.keras.models.load_model(r"/home/aarush100616/Downloads/Projects/Cats & Dogs Classifier/cat_vs_dog_CNN.model")
prediction=model.predict([prepare(image)])
print(Categories[int(prediction[0][0])])
img=mpimg.imread(image)
imgplot=plt.imshow(img)
plt.title(Categories[int(prediction[0][0])])
plt.show()

