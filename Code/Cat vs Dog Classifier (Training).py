#!/usr/bin/env python
# coding: utf-8

# # Cat vs Dog Classifier(Training Model)
# #By- Aarush Kumar
# #Dated: June 01,2021

# In[1]:


import tensorflow as tf
import numpy as np
from keras.models import Sequential
from keras.layers import Dense,Dropout,Activation,Flatten
import pickle
from keras.layers import Conv2D,MaxPooling2D


# In[2]:


pickle_in=open(r"/home/aarush100616/Downloads/Projects/Cats & Dogs Classifier/X.pickle","rb")
X=pickle.load(pickle_in)

pickle_in=open(r"/home/aarush100616/Downloads/Projects/Cats & Dogs Classifier/y.pickle","rb")
y=pickle.load(pickle_in)


# In[3]:


print(X)


# In[4]:


print(y)


# In[5]:


y=np.array(y)


# In[6]:


X=X/255.0
print(X)


# In[7]:


model= Sequential()


# In[8]:


model.add(Conv2D(256,(3,3),input_shape=X.shape[1:]))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))


# In[9]:


model.add(Conv2D(256,(3,3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))


# In[10]:


model.add(Flatten())
model.add(Dense(64))


# In[11]:


model.add(Dense(1))
model.add(Activation('sigmoid'))


# In[12]:


model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.fit(X,y,batch_size=4,epochs=10,validation_split=0.3)


# In[13]:


model.save(r"/home/aarush100616/Downloads/Projects/Cats & Dogs Classifier/cat_vs_dog_CNN.model")

